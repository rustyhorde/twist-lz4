//! lz4 compression extension for `twist`
extern crate lz4_compress;

#[macro_use]
extern crate slog;
#[macro_use]
extern crate twist;

mod client;
mod server;
mod util;

/// This extension needs to use the `rsv2` bit in a websocket frame.
const RSV2: u8 = 2;
/// The `Sec-WebSocket-Extensions` header prefix.
const SWE: &'static str = "Sec-WebSocket-Extensions: ";
/// The `permessage-lz4` header value.
const PMLZ4: &'static str = "permessage-lz4";

pub use server::Lz4 as ServerLz4;
pub use client::Lz4 as ClientLz4;
